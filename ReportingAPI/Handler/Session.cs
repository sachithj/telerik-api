﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportingAPI.Handler
{
    public class Session
    {
        public int Pid { get; set; }
        public object CustomerId { get; set; }
        public object ContactId { get; set; }
        public string SessionId { get; set; }
        public string Database { get; set; }
        public string RamBaseName { get; set; }
        public string GatewayIp { get; set; }
        public string GatewayHostName { get; set; }
    }
}
