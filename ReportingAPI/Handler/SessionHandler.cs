﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RambaseApiSDK;

namespace ReportingAPI.Handler
{
    public class SessionHandler
    {
        private readonly RambaseApi api;
        private readonly string sessionResourceUrl;


        public SessionHandler()
        {
            //Get new credentials for rambase sdk
            string clientId = "smMbS3flXkCmB9u9-YeDzg2";
            string clientSecret = "zFukr3tgG0yZDQHdOmEOUQ2";

            sessionResourceUrl = "system/sessions/current";

            try
            {
                api = new RambaseApi(clientId, clientSecret);
            }
            catch (Exception ex)
            {

            }
        }

        /// <summary>
        /// Get current session details
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public Session GetCurrentSession(string accessToken)
        {
            if (string.IsNullOrEmpty(accessToken))
            {
                return null;
            }
            api.AccessToken = accessToken;

            Session session = null;

            ApiResponse apiResponse = api.SendRequest(APIResourceVerb.GET, sessionResourceUrl);

            if (!apiResponse.HasErrors)
                session = apiResponse.ToObject<Session>();

            return session;
        }


        public bool IsValidToken(string token)
        {
            Session session = GetCurrentSession(token);

            if (session != null)
                return true;
            return false;
        }
    }
}
