﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ReportingAPI.Entities;

namespace ReportingAPI.Handler
{
	public class HttpHandler
	{
		private string AccessToken;

		public HttpHandler(string accessToken)
		{
			AccessToken = accessToken;
		}

		public EditFileCheckSum GetEditFileCheckSum(string uri)
		{
			try
			{
				string url = SetAccessToken(uri);
				HttpWebRequest request = (HttpWebRequest) WebRequest.Create(url);
				request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

				using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
				using (Stream stream = response.GetResponseStream())
				using (StreamReader reader = new StreamReader(stream))
				{
					EditFileCheckSum checkSum = JsonConvert.DeserializeObject<EditFileCheckSum>(reader.ReadToEnd());

					return checkSum;
				}
			}
			catch (Exception ex)
			{
				return null;
			}
		}

		public ProductionFileChecksum GetProductionCheckSum(string uri)
		{
			try
			{
				string url = SetAccessToken(uri);
				HttpWebRequest request = (HttpWebRequest) WebRequest.Create(url);
				request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

				using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
				using (Stream stream = response.GetResponseStream())
				using (StreamReader reader = new StreamReader(stream))
				{
					ProductionFileChecksum checkSum = JsonConvert.DeserializeObject<ProductionFileChecksum>(reader.ReadToEnd());

					return checkSum;
				}
			}
			catch(Exception ex)
			{
				return null;
			}
		}


		/// <summary>
		/// Set access token query in to the URL
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		public string SetAccessToken(string url)
		{
			string fullUrl = string.Format($"{url}?$access_token={AccessToken}");
			return fullUrl;
		}
	}
}
