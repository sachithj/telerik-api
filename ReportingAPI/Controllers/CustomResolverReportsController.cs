﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Telerik.Reporting;
using Telerik.Reporting.Services;
using Telerik.Reporting.Services.AspNetCore;


namespace ReportingAPI.Controllers
{
    public class CustomResolverReportsController : ReportsControllerBase
    {
        static Telerik.Reporting.Services.ReportServiceConfiguration configurationInstance;

        static CustomResolverReportsController()
        {
            var resolver = new ReportFileResolver("~/Reports")
                .AddFallbackResolver(new ReportTypeResolver());
            configurationInstance = new Telerik.Reporting.Services.ReportServiceConfiguration
            {
                HostAppId = "Application1",
                ReportResolver = resolver,
                Storage = new Telerik.Reporting.Cache.File.FileStorage(),
            };
        }

        public CustomResolverReportsController()
        {
            ReportServiceConfiguration = configurationInstance;
        }
    }
}