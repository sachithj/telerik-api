﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using ReportingAPI.Entities;
using Newtonsoft.Json;
using ReportingAPI.Handler;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Telerik.Reporting;
using Telerik.Reporting.Drawing;
using Telerik.Reporting.Processing;
using TelerikRuntime;
using TelerikRuntime.Common;
using TelerikRuntime.Common.Entities;

namespace ReportingAPI.Controllers
{
    public class CustomReportResolver : Telerik.Reporting.Services.Engine.IReportResolver
    {
        private readonly IWebHostEnvironment env;
        private readonly IConfiguration configuration;


        public CustomReportResolver(IWebHostEnvironment env, IConfiguration configuration)
        {
            this.env = env;
            this.configuration = configuration;
        }

        public ReportSource Resolve(string metaDataString)
        {
            if (!string.IsNullOrEmpty(metaDataString)) {
                SessionHandler sessionHandler = new SessionHandler();
                dynamic metaData = JsonConvert.DeserializeObject<dynamic>(metaDataString);
                string accessToken = metaData.accessToken;

                //Create new CultureInfo
                var cultureInfo = new System.Globalization.CultureInfo("no");
                System.Threading.Thread.CurrentThread.CurrentUICulture = cultureInfo;
                System.Threading.Thread.CurrentThread.CurrentCulture = cultureInfo;

                string pid = metaData.versionData.createdBy.employeeId;
                string link = metaData.versionData.outputDesignVersionLink;
                string opdId = link.Split("/")[5];
                string versionId = metaData.versionData.outputDesignVersionId;
                string status = metaData.versionData.status;

                Handler.HttpHandler httpHandler = new Handler.HttpHandler(accessToken);

                string name = null;

                if (status.Equals("1"))
                {
                    string url = string.Format(this.configuration["OutputRepository:getEditCheckSum"], opdId, versionId);

                    EditFileCheckSum checkSum = httpHandler.GetEditFileCheckSum(url);
                    string editUri = checkSum.FileUrls.FirstOrDefault().FileUrl;
                    editUri = editUri.Split("/").Last();
                    editUri = editUri.Split(".").First();
                    name = editUri;
                }
                if (status.Equals("2") || status.Equals("4"))
                {
                    string url = string.Format(this.configuration["OutputRepository:getCheckSum"], opdId, versionId);

                    ProductionFileChecksum checkSum = httpHandler.GetProductionCheckSum(url);
                    name = checkSum.EncodedValue;
                }

                MetaFileData metaFileData = new MetaFileData()
                {
                    DesginVersionId = Convert.ToInt32(versionId),
                    Language = "EN",
                    Localization = "EN-US",
                    Name = name,
                    OpdId = Convert.ToInt32(opdId),
                    OutputType = "PDF",
                    PrimaryLogo = "104469",
                    ReportPath = "D:\\RambaseReports\\",
                    Style = "104560.xml",
                    Target = "JHCDEVSYS"
                };

                var checksumFileReader = new ChecksumFileReader();
                FileResult fileAvailable = checksumFileReader.GetFileFromDisk(metaFileData, accessToken);

                if (fileAvailable.Result)
                {
                    var definitionFile = checksumFileReader.GetDefinitionFile();
                    using var sourceStream = File.OpenRead(Path.Combine(env.ContentRootPath, definitionFile));
                    //using var sourceStream = File.OpenRead(Path.Combine(env.ContentRootPath, "Reports/Report4.trdp"));

                    var reportPackager = new ReportPackager();
                    var report = (Telerik.Reporting.Report)reportPackager.UnpackageDocument(sourceStream);
                    var instanceReportSource = new InstanceReportSource
                    {
                        ReportDocument = report
                    };

                    return instanceReportSource;
                }
            }
            throw new Exception("File not found");
        }
    }
}
