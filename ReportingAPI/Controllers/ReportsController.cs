﻿namespace AspNetCoreDemo.Controllers
{
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using ReportingAPI.Controllers;
    using System.Net;
    using System.Net.Mail;
    using Telerik.Reporting.Services;
    using Telerik.Reporting.Services.AspNetCore;

    [Route("api/reports")]
    public class ReportsController : ReportsControllerBase
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfiguration _configuration;

        public ReportsController(IReportServiceConfiguration reportServiceConfiguration, IWebHostEnvironment env, IConfiguration configuration)
          : base(reportServiceConfiguration)
        {
            _env = env;
            _configuration = configuration;

            reportServiceConfiguration.ReportResolver = new CustomReportResolver(_env, _configuration);
        }


        protected override HttpStatusCode SendMailMessage(MailMessage mailMessage)
        {
            throw new System.NotImplementedException("This method should be implemented in order to send mail messages");
        }
    }
}