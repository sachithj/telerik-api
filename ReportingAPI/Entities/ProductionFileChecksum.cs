﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportingAPI.Entities
{
	public class ProductionFileChecksum
	{
		public string RepositoryId { get; set; }
		public string EncodedValue { get; set; }
	}
}
