﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportingAPI.Entities
{
	public class EditFileCheckSum
	{
		public List<EditFile> FileUrls { get; set; }
	}

	public class EditFile
	{
		public string FileUrl { get; set; }
	}
}
